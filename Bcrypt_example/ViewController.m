//
//  ViewController.m
//  Bcrypt_example
//
//  Created by Yaroslav Brekhunchenko on 11/20/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "Bcrypt_example-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString* saltString = BCryptSwift.generateSalt;
    NSLog(@"Salt string %@", saltString);
    
    NSInteger numberOfRounds = 4;
    saltString = [BCryptSwift generateSaltWithNumberOfRounds:numberOfRounds];
    NSLog(@"Salt string %@ with number of rounds %lu", saltString, numberOfRounds);
    
    NSString* hashPassword = [BCryptSwift hashPassword:@"qwerty" withSalt:saltString];
    NSLog(@"hashPassword %@", hashPassword);
    
    BOOL correctPassword = [BCryptSwift verifyPassword:@"qwerty" matchesHash:hashPassword];
    NSLog(@"Password %@", correctPassword ? @"correct" : @"wrong");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
